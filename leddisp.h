#ifndef _leddisp_H_
#define  _leddisp_H_

#define LEDDISPNO	4	//liczba cyfr

#define DDR_LED1 DDRB
#define DDR_LED2 DDRD
#define PORT_LED1 PORTB
#define PORT_LED2 PORTD
#define DDR_KATODY DDRC
#define PORT_KATODY PORTC


#define SEG_A	0	//numer wykorzystanego wyprowadzenia na poszczegolnym porcie
#define SEG_B	1
#define SEG_C	2
#define SEG_D	3
#define SEG_E	4
#define SEG_F	5
#define SEG_G	7

#define KAT_A	0	//numer wyprowadzenia sterujacych poszczegolnymi katodami
#define KAT_B	1
#define KAT_C	2
#define KAT_D	3
#define STOPIEN 10
#define CELSJUSZA 11

void LED_init(void);

extern uint8_t LEDDIGITS[LEDDISPNO];	//bufor na wyswietlane cyfry

#endif /*_leddisp_H_*/

