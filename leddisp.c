#include <stdio.h>
#include <avr/pgmspace.h>
#include "leddisp.h"
#include <avr/io.h>
#include <avr/interrupt.h>


//Cyfry 0,1,2,3,4,5,6,7,8,9, symbol stopnia, C
static const uint8_t PROGMEM DIGITS[12]={0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, 0x63, 0x39};

uint8_t LEDDIGITS[LEDDISPNO] = {0,0,STOPIEN,CELSJUSZA};	//bufor na wyswietlane cyfry, inicjalizacja 3 i 4 cyfry znakami stopnia Celsjusza

static inline void ShowOnLED(uint8_t val){
	uint8_t tmp = 0;	
	if((val & 0x7f) < 12) tmp = pgm_read_byte(&DIGITS[val & 0x7f]);
	PORT_LED1 = (PORT_LED1 & 0xC0) | (tmp & 0x3f);
	PORT_LED2 = (PORT_LED2 & 0x7F) | ((tmp & 0x40)<<1);	//segment g na porcie PD7
}

void LED_init(){
	DDR_KATODY |= 1<<KAT_A | 1<<KAT_B | 1<<KAT_C | 1<<KAT_D;
	PORT_KATODY &= ~(1<<KAT_A | 1<<KAT_B | 1<<KAT_C | 1<<KAT_D);	 
	DDR_LED1 |= 1<<SEG_A | 1<<SEG_B | 1<<SEG_C | 1<<SEG_D | 1<<SEG_E | 1<<SEG_F;
	DDR_LED2 |= 1<<SEG_G;
	PORT_LED1 &= ~(1<<SEG_A | 1<<SEG_B | 1<<SEG_C | 1<<SEG_D | 1<<SEG_E | 1<<SEG_F);
	PORT_LED2 &= ~(1<<SEG_G);	
	TCCR2 |= 1<<WGM21 | 1<<CS22 | 1<<CS21 | 1<<CS20;	//timer2, tryb CTC, prescaler=1024
	OCR2 = 77;	//czestotliwosc przerwania ~200Hz
	TIMSK |= 1<<OCIE2;	//aktywacja przerwan w trybie CTC
}

ISR(TIMER2_COMP_vect){
	static uint8_t LEDNO;
	PORT_KATODY &= ~(1<<KAT_A | 1<<KAT_B | 1<<KAT_C | 1<<KAT_D);	//wygas wyswietlacze
	LEDNO = (LEDNO+1) % LEDDISPNO;	//wybor kolejnej cyfry (katody) do wyswietlenia
	ShowOnLED(LEDDIGITS[LEDNO]);	//wybierz cyfre do wyswietlenia (poszczegolne segmenty)
	if(LEDNO!=0 || LEDDIGITS[0]!=0) PORT_KATODY |= 1<<LEDNO;	//wlaczenie nastepnej cyfry	(warunek if usuwa nieznaczace zero)
}
