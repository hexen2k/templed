/*
 * templed.c
 *
 * Created: 2014-01-06 14:40:29
 *  Author: hexen2k
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include <util/atomic.h>
#include "leddisp.h"
#include "uart.h"
#include "onewire.h"
#include "ds18x20.h"

#define TEMPALARM	90	//temperatura po przekroczeniu ktorej aktywuje sie alarm dzwiekowy
#define TIMEBEEP_H 5	//x 10 ms (okres Timera1)
#define TIMEBEEP_L 200
#define TIMEREFRESH 6000	//timeout dla modulu radiowego
#define TIMEDISPLAYRADIO 1000	//czas wyswietlania danych z radia
#define TIMEDISPLAYONEWIRE 200	//czas wyswietlania temperatury z czujnika
#define TIMEOWCONVERSION 75		//750ms na konwersje temperatury w czujniku ds18b20
#define TIMEALARMDISABLE 60000	//10min timeout dla alarmu
#define UART_BAUD_RATE	2400

#define STARTFRAME 0xF0

#define BUZER (1<<PC4)
#define KEY (1<<PD6)
#define LEDALARM (1<<PC5)
#define LEDREFRESH (1<<PD5)
#define LEDONEWIRE (1<<PD3)

#define MAXSENSORS 1	//maksymalna liczba czujnikow podlaczanych do magistrali

uint8_t gSensorIDs[MAXSENSORS][OW_ROMCODE_SIZE];
uint16_t character;
int16_t decicelsius;	//99,3 stopnia -> 993
int8_t decicelsius_mod;	//293 -> 29; 345 -> 35; jesli czesc ulamkowa >= 5, zaokraglaj w gore
uint8_t temperatura, temperatura_temp, sound_flag, alarm=1, conversion_flag, ow_fresh_flag, display_flag=1, temperatura_temp_neg;
volatile uint8_t timer1, timer2;	//timer1 - opoznienia generowania dzwieku, timer2 - opoznienie odczytu OneWire
volatile uint16_t timer3, timer4, timer5;			//timer3 - opoznienie do sygnalizacji braku danych z radia, timer4 - opoznienie do przelaczania zawartosci wyswietlacza, timer5 - czasowa deaktywacja alarmu
uint16_t timer4_tmp;

static uint8_t search_sensors(void);
static int8_t decicelsius_correct(int16_t val);

int main(void)
{	
	uart_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) ); 
	LED_init();
	DDRC |= BUZER | LEDALARM;	//wyjscie: buzer i dioda alarmu	
	PORTC |= LEDALARM;			//wygas diode (logika ujemna)
	DDRD &= ~KEY;	//wejscie: przycisk kasowania alarmu
	DDRD |= LEDREFRESH | LEDONEWIRE;	//wyjsice: dioda odswiezenia
	PORTD |= KEY;	//pullup na przycisku

	TCCR1B |= 1<<WGM12 | 1<<CS10 | 1<<CS12;	//Timer 1 tryb CTC, preskaler 1024
	OCR1A = 156;	//przerwanie co ~10ms
	TIMSK |= 1<<OCIE1A;
	
	wdt_enable(WDTO_2S);	//aktywuj watchdoga - timeout ~2s
	sei();
			
    while(1){
			
		character = uart_getc();
		if(character == STARTFRAME){
			while((character=uart_getc()) == UART_NO_DATA);	// czekaj na kolejny znak
			temperatura_temp = character;
			while((character=uart_getc()) == UART_NO_DATA);	// czekaj na kolejny znak
			temperatura_temp_neg = character;
			temperatura_temp_neg = ~temperatura_temp_neg;	//ostatni znak zawiera prosta sume kontrolna (negacje bitowa poprzedniego bajtu)
			if(temperatura_temp == temperatura_temp_neg){					
				if(temperatura_temp <= 99 && temperatura_temp >= 2){	//test poprawnosci zakresu
					temperatura=temperatura_temp;	//dodatkowe przepisanie temperatury (po weryfikacji znaku kontrolnego)
					ATOMIC_BLOCK(ATOMIC_FORCEON){
						timer3 = TIMEREFRESH;	//przeladuj timer po otrzymaniu nowych danych
					}
					PORTD |= LEDREFRESH;	//wygas diode
				}
			}						
		}
		
		if( !(PIND & KEY) ){	//zalaczanie lub wylaczanie alarmu 									
			alarm ^= 1;
			if(alarm) PORTC |= LEDALARM;	//wylacz diode alarmu (podczas wlaczonego alarmu dioda nie swieci!)
				else { //(!alarm)
					PORTC &= ~(LEDALARM);
					ATOMIC_BLOCK(ATOMIC_FORCEON){
						timer5 = TIMEALARMDISABLE;	//ustaw timeout dla wylaczonego alarmu
					}
				}
		_delay_ms(400);
		}
		
		ATOMIC_BLOCK(ATOMIC_FORCEON){
			if(!timer5){	//po uplywie timeoutu wlacz ponownie alarm + wylacz diode alarmu
				alarm = 1;
				PORTC |= LEDALARM;
			}
		}					
		
		if(alarm && temperatura>TEMPALARM){	//sterowanie buzerem
			if(!timer1){
				if(!sound_flag){
					PORTC |= BUZER;
					sound_flag = 1;
					timer1 = TIMEBEEP_H;
				} else {
					PORTC &= ~(BUZER);
					sound_flag = 0;
					timer1 = TIMEBEEP_L - (temperatura%10*20);	//zwiekszanie czestotliwosci sygnalu alarmowego
				}	
			}
		} else PORTC &= ~(BUZER);		
				
		if(!timer2 && !conversion_flag){	//jesli odliczono czas i w czujniku nie trwa konwersja
			if(search_sensors()){		//jesli znaleziono czujnik
				if(DS18X20_start_meas(DS18X20_POWER_PARASITE,NULL) == DS18X20_OK){	//jesli poprawnie zainicjowano konwersje temperatury
					conversion_flag = 1;	//ustaw flage konwersji temperatury
					timer2 = TIMEOWCONVERSION;	//odczekaj na zakonczenie konwersji temperatury w czujniku
				}
			}
		} else if(!timer2 && conversion_flag){	//odliczono 750ms po wyszukaniu czujnika i zainicjalizowaniu konwersji temperatury
			if (DS18X20_read_decicelsius(&gSensorIDs[0][0], &decicelsius) == DS18X20_OK){
				ow_fresh_flag = 1;	//odczyt danych z czujnika + ustawienie flagi swiezosci danych								
				decicelsius_mod = decicelsius_correct(decicelsius);	//formatuj dane - patrz komentarze zmiennych
			}
			conversion_flag = 0;
		}
		
		ATOMIC_BLOCK(ATOMIC_FORCEON){
			timer4_tmp = timer4;
		}		
	
		if(!timer4_tmp){
			if(display_flag){	//pokaz temperature z czujnika ds18b20
				if(ow_fresh_flag){										
					LEDDIGITS[1]=decicelsius_mod%10;
					LEDDIGITS[0]=decicelsius_mod/10;					
					PORTD |= LEDONEWIRE;	//wlacz diode sygnalizujaca wyswietlanie temperatury z czujninka ds18b20
					display_flag = 0;
					ow_fresh_flag = 0;	
				}	//jesli nie ma danych z czujnika ds18b20 (ow_fresh_flag==1) wyswietlaj dane z radia (display_flag==1)				
				ATOMIC_BLOCK(ATOMIC_FORCEON){
					timer4 = TIMEDISPLAYONEWIRE;
				}							
			} else {	//(!display_flag)	//pokaz dane z radia
				PORTD &= ~LEDONEWIRE;
				LEDDIGITS[0]=temperatura/10;
				LEDDIGITS[1]=temperatura%10;
				ATOMIC_BLOCK(ATOMIC_FORCEON){
					timer4 = TIMEDISPLAYRADIO;
				}
				display_flag = 1;
			}			
		} else if(display_flag){	//pokaz dane z radia (tutaj odswiezane na biezaco)
			LEDDIGITS[0]=temperatura/10;
			LEDDIGITS[1]=temperatura%10;
		}
				
		ATOMIC_BLOCK(ATOMIC_FORCEON){
			if(!timer3) PORTD &= ~(LEDREFRESH);	//po odmierzeniu czasu i braku nowych danych wlacz diody informacyjne
		}
		
		wdt_reset();	//resetuj watchdoga		
    }
}

ISR(TIMER1_COMPA_vect){
	if(timer1) timer1--;
	if(timer2) timer2--;
	if(timer3) timer3--;
	if(timer4) timer4--;
	if(timer5) timer5--;
}

//funkcja wyszukujaca czujniki na magistrali, zwraca liczbe znalezionych czujnikow
static uint8_t search_sensors(void)
{
	uint8_t i;
	uint8_t id[OW_ROMCODE_SIZE];
	uint8_t diff, nSensors;

	ow_reset();
	nSensors = 0;

	diff = OW_SEARCH_FIRST;
	while ( diff != OW_LAST_DEVICE && nSensors < MAXSENSORS ) {
		DS18X20_find_sensor( &diff, &id[0] );

		if( diff == OW_PRESENCE_ERR ||  diff == OW_DATA_ERR) break;

		for ( i=0; i < OW_ROMCODE_SIZE; i++ )
		gSensorIDs[nSensors][i] = id[i];

		nSensors++;
	}

	return nSensors;
}

static int8_t decicelsius_correct(int16_t val){
	int8_t temp;
	temp = val/10;
	if((val%10) >= 5) temp++;
	return temp;
}
